package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.ConsultasApp;

import pe.gob.trabajo.domain.Estadopase;
import pe.gob.trabajo.repository.EstadopaseRepository;
import pe.gob.trabajo.repository.search.EstadopaseSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EstadopaseResource REST controller.
 *
 * @see EstadopaseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConsultasApp.class)
public class EstadopaseResourceIntTest {

    private static final String DEFAULT_V_NOMESTPAS = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMESTPAS = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private EstadopaseRepository estadopaseRepository;

    @Autowired
    private EstadopaseSearchRepository estadopaseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEstadopaseMockMvc;

    private Estadopase estadopase;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EstadopaseResource estadopaseResource = new EstadopaseResource(estadopaseRepository, estadopaseSearchRepository);
        this.restEstadopaseMockMvc = MockMvcBuilders.standaloneSetup(estadopaseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Estadopase createEntity(EntityManager em) {
        Estadopase estadopase = new Estadopase()
            .vNomestpas(DEFAULT_V_NOMESTPAS)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return estadopase;
    }

    @Before
    public void initTest() {
        estadopaseSearchRepository.deleteAll();
        estadopase = createEntity(em);
    }

    @Test
    @Transactional
    public void createEstadopase() throws Exception {
        int databaseSizeBeforeCreate = estadopaseRepository.findAll().size();

        // Create the Estadopase
        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isCreated());

        // Validate the Estadopase in the database
        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeCreate + 1);
        Estadopase testEstadopase = estadopaseList.get(estadopaseList.size() - 1);
        assertThat(testEstadopase.getvNomestpas()).isEqualTo(DEFAULT_V_NOMESTPAS);
        assertThat(testEstadopase.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testEstadopase.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testEstadopase.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testEstadopase.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testEstadopase.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testEstadopase.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testEstadopase.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Estadopase in Elasticsearch
        Estadopase estadopaseEs = estadopaseSearchRepository.findOne(testEstadopase.getId());
        assertThat(estadopaseEs).isEqualToComparingFieldByField(testEstadopase);
    }

    @Test
    @Transactional
    public void createEstadopaseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = estadopaseRepository.findAll().size();

        // Create the Estadopase with an existing ID
        // estadopase.setId(1L);
        estadopase.setId("1");

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        // Validate the Estadopase in the database
        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvNomestpasIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadopaseRepository.findAll().size();
        // set the field null
        estadopase.setvNomestpas(null);

        // Create the Estadopase, which fails.

        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadopaseRepository.findAll().size();
        // set the field null
        estadopase.setnUsuareg(null);

        // Create the Estadopase, which fails.

        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadopaseRepository.findAll().size();
        // set the field null
        estadopase.settFecreg(null);

        // Create the Estadopase, which fails.

        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadopaseRepository.findAll().size();
        // set the field null
        estadopase.setnFlgactivo(null);

        // Create the Estadopase, which fails.

        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadopaseRepository.findAll().size();
        // set the field null
        estadopase.setnSedereg(null);

        // Create the Estadopase, which fails.

        restEstadopaseMockMvc.perform(post("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isBadRequest());

        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEstadopases() throws Exception {
        // Initialize the database
        estadopaseRepository.saveAndFlush(estadopase);

        // Get all the estadopaseList
        restEstadopaseMockMvc.perform(get("/api/estadopases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            // .andExpect(jsonPath("$.[*].id").value(hasItem(estadopase.getId().intValue())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estadopase.getId())))
            .andExpect(jsonPath("$.[*].vNomestpas").value(hasItem(DEFAULT_V_NOMESTPAS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getEstadopase() throws Exception {
        // Initialize the database
        estadopaseRepository.saveAndFlush(estadopase);

        // Get the estadopase
        restEstadopaseMockMvc.perform(get("/api/estadopases/{id}", estadopase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            // .andExpect(jsonPath("$.id").value(estadopase.getId().intValue()))
            .andExpect(jsonPath("$.id").value(estadopase.getId()))
            .andExpect(jsonPath("$.vNomestpas").value(DEFAULT_V_NOMESTPAS.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingEstadopase() throws Exception {
        // Get the estadopase
        restEstadopaseMockMvc.perform(get("/api/estadopases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstadopase() throws Exception {
        // Initialize the database
        estadopaseRepository.saveAndFlush(estadopase);
        estadopaseSearchRepository.save(estadopase);
        int databaseSizeBeforeUpdate = estadopaseRepository.findAll().size();

        // Update the estadopase
        Estadopase updatedEstadopase = estadopaseRepository.findOne(estadopase.getId());
        updatedEstadopase
            .vNomestpas(UPDATED_V_NOMESTPAS)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restEstadopaseMockMvc.perform(put("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEstadopase)))
            .andExpect(status().isOk());

        // Validate the Estadopase in the database
        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeUpdate);
        Estadopase testEstadopase = estadopaseList.get(estadopaseList.size() - 1);
        assertThat(testEstadopase.getvNomestpas()).isEqualTo(UPDATED_V_NOMESTPAS);
        assertThat(testEstadopase.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testEstadopase.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testEstadopase.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testEstadopase.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testEstadopase.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testEstadopase.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testEstadopase.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Estadopase in Elasticsearch
        Estadopase estadopaseEs = estadopaseSearchRepository.findOne(testEstadopase.getId());
        assertThat(estadopaseEs).isEqualToComparingFieldByField(testEstadopase);
    }

    @Test
    @Transactional
    public void updateNonExistingEstadopase() throws Exception {
        int databaseSizeBeforeUpdate = estadopaseRepository.findAll().size();

        // Create the Estadopase

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEstadopaseMockMvc.perform(put("/api/estadopases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estadopase)))
            .andExpect(status().isCreated());

        // Validate the Estadopase in the database
        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEstadopase() throws Exception {
        // Initialize the database
        estadopaseRepository.saveAndFlush(estadopase);
        estadopaseSearchRepository.save(estadopase);
        int databaseSizeBeforeDelete = estadopaseRepository.findAll().size();

        // Get the estadopase
        restEstadopaseMockMvc.perform(delete("/api/estadopases/{id}", estadopase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean estadopaseExistsInEs = estadopaseSearchRepository.exists(estadopase.getId());
        assertThat(estadopaseExistsInEs).isFalse();

        // Validate the database is empty
        List<Estadopase> estadopaseList = estadopaseRepository.findAll();
        assertThat(estadopaseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEstadopase() throws Exception {
        // Initialize the database
        estadopaseRepository.saveAndFlush(estadopase);
        estadopaseSearchRepository.save(estadopase);

        // Search the estadopase
        restEstadopaseMockMvc.perform(get("/api/_search/estadopases?query=id:" + estadopase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            // .andExpect(jsonPath("$.[*].id").value(hasItem(estadopase.getId().intValue())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estadopase.getId())))
            .andExpect(jsonPath("$.[*].vNomestpas").value(hasItem(DEFAULT_V_NOMESTPAS.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Estadopase.class);
        Estadopase estadopase1 = new Estadopase();
        // estadopase1.setId(1L);
        estadopase1.setId("1");
        Estadopase estadopase2 = new Estadopase();
        estadopase2.setId(estadopase1.getId());
        assertThat(estadopase1).isEqualTo(estadopase2);
        // estadopase2.setId(2L);
        estadopase2.setId("2");
        assertThat(estadopase1).isNotEqualTo(estadopase2);
        estadopase1.setId(null);
        assertThat(estadopase1).isNotEqualTo(estadopase2);
    }
}
