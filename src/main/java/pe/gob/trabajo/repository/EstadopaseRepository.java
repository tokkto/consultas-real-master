package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Estadopase;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the Estadopase entity.
 */
@SuppressWarnings("unused")
@Repository
// public interface EstadopaseRepository extends JpaRepository<Estadopase, Long> {
    public interface EstadopaseRepository extends JpaRepository<Estadopase, String> {

    // Lista todos los pases activos del sistema
    @Query("select estadopase from Estadopase estadopase where estadopase.nFlgactivo = true")
    List<Estadopase> findAll_Activos();

}
