package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Estadopase;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Estadopase entity.
 */
// public interface EstadopaseSearchRepository extends ElasticsearchRepository<Estadopase, Long> {
public interface EstadopaseSearchRepository extends ElasticsearchRepository<Estadopase, String> {
}
