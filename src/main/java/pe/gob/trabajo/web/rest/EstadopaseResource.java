package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Estadopase;

import pe.gob.trabajo.repository.EstadopaseRepository;
import pe.gob.trabajo.repository.search.EstadopaseSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Estadopase.
 */
@RestController
@RequestMapping("/api")
public class EstadopaseResource {

    private final Logger log = LoggerFactory.getLogger(EstadopaseResource.class);

    private static final String ENTITY_NAME = "estadopase";

    private final EstadopaseRepository estadopaseRepository;

    private final EstadopaseSearchRepository estadopaseSearchRepository;

    public EstadopaseResource(EstadopaseRepository estadopaseRepository, EstadopaseSearchRepository estadopaseSearchRepository) {
        this.estadopaseRepository = estadopaseRepository;
        this.estadopaseSearchRepository = estadopaseSearchRepository;
    }

    /**
     * POST  /estadopases : Create a new estadopase.
     *
     * @param estadopase the estadopase to create
     * @return the ResponseEntity with status 201 (Created) and with body the new estadopase, or with status 400 (Bad Request) if the estadopase has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/estadopases")
    @Timed
    public ResponseEntity<Estadopase> createEstadopase(@Valid @RequestBody Estadopase estadopase) throws URISyntaxException {
        log.debug("REST request to save Estadopase : {}", estadopase);
        if (estadopase.getId() != null) {
            throw new BadRequestAlertException("A new estadopase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Estadopase result = estadopaseRepository.save(estadopase);
        estadopaseSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/estadopases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /estadopases : Updates an existing estadopase.
     *
     * @param estadopase the estadopase to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated estadopase,
     * or with status 400 (Bad Request) if the estadopase is not valid,
     * or with status 500 (Internal Server Error) if the estadopase couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/estadopases")
    @Timed
    public ResponseEntity<Estadopase> updateEstadopase(@Valid @RequestBody Estadopase estadopase) throws URISyntaxException {
        log.debug("REST request to update Estadopase : {}", estadopase);
        if (estadopase.getId() == null) {
            return createEstadopase(estadopase);
        }
        Estadopase result = estadopaseRepository.save(estadopase);
        estadopaseSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, estadopase.getId().toString()))
            .body(result);
    }

    /**
     * GET  /estadopases : get all the estadopases.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of estadopases in body
     */
    @GetMapping("/estadopases")
    @Timed
    public List<Estadopase> getAllEstadopases() {
        log.debug("REST request to get all Estadopases");
        return estadopaseRepository.findAll();
        }

    /**
     * GET  /estadopases/:id : get the "id" estadopase.
     *
     * @param id the id of the estadopase to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the estadopase, or with status 404 (Not Found)
     */
    @GetMapping("/estadopases/{id}")
    @Timed
    // public ResponseEntity<Estadopase> getEstadopase(@PathVariable Long id) {
    public ResponseEntity<Estadopase> getEstadopase(@PathVariable String id) {        
        log.debug("REST request to get Estadopase : {}", id);
        Estadopase estadopase = estadopaseRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(estadopase));
    }

    /** JH
     * GET  /estadopases/activos : get all the estadopases.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of estadopases in body
     */
    @GetMapping("/estadopases/activos")
    @Timed
    public List<Estadopase> getAll_Activos() {
        log.debug("REST request to get all estadopases");
        return estadopaseRepository.findAll_Activos();
    }

    /**
     * DELETE  /estadopases/:id : delete the "id" estadopase.
     *
     * @param id the id of the estadopase to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/estadopases/{id}")
    @Timed
    // public ResponseEntity<Void> deleteEstadopase(@PathVariable Long id) {
    public ResponseEntity<Void> deleteEstadopase(@PathVariable String id) {
        log.debug("REST request to delete Estadopase : {}", id);
        estadopaseRepository.delete(id);
        estadopaseSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/estadopases?query=:query : search for the estadopase corresponding
     * to the query.
     *
     * @param query the query of the estadopase search
     * @return the result of the search
     */
    @GetMapping("/_search/estadopases")
    @Timed
    public List<Estadopase> searchEstadopases(@RequestParam String query) {
        log.debug("REST request to search Estadopases for query {}", query);
        return StreamSupport
            .stream(estadopaseSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
